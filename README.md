## Sample Custom provider app

In order to use this app, do the following
```bash
$ cd node_modules/
$ git clone https://gitlab.com/iyerrama29/contentstack-express.git
$ cd contentstack-express
$ npm install
$ cd ../../
$ npm start
```
Please make sure to add in your stack details @`config/all.js`

You can find the custom mongo provider under `providers/` directory

## Changes in version 0.0.1

Custom mongo provider is internal