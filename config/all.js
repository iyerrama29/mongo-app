var _path = require('path');

module.exports = exports = {
    port: "4000",
    theme: "basic",
    languages: [
        {
            "code": "en-us",
            "relative_url_prefix": "/"
        },
        {
            "code": "ja-jp",
            "relative_url_prefix": "/ja-jp/"
        }
    ],
    plugins: {
        myplugin: {}
    },
    cache: false,
    storage: {
        // provider: "cdn"
        provider: "mongodb",
        options: {
                hostname: "localhost",
                port: "27017"
        }
    },
    contentstack: {
        api_key: "blt0a3e0fa8bf2766a9",
        access_token: "blt4a56deb2e209f917"
    }
};